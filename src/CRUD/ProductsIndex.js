import axios from "axios";
import { useState, useEffect } from "react";

import React from 'react'
import { Link } from "react-router-dom";

export default function ProductsIndex() {

    const [products,setProducts] = useState([]);

    function GetProducts(){
        axios({
            method:"GET",
            url:"http://127.0.0.1:5000/products"

        }).then(response=>{
            setProducts(response.data);
        })
    }

    useEffect(()=>{
        GetProducts();
    },[])

  return (
    <div className="container-fluid">
        <h2>Products Data</h2>
            <table
                className="table table-hover"
            >
                <thead>
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Price</th>
                        <th scope="col">Stock</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        products.map(product=>
                            <tr>
                                <td>{product.Name}</td>
                                <td>{product.Price}</td>
                                <td>{(product.Stock==true)?"Available":"Out Of Stock"}</td>
                            </tr>
                        )
                    }
                </tbody>
            </table>
            <div>
                <Link to="/registerproduct">Add New Product</Link>
            </div>
    </div>
  )
}
