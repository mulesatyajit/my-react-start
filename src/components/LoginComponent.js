import  axios  from "axios";
import { useFormik } from "formik";
import React from "react";
import { Link } from "react-router-dom";
import {useNavigate} from 'react-router-dom';

export default function LoginComponent() {

    const navigate = useNavigate();

    const formik = useFormik({
        initialValues:{
            UserName:'',
            Password:''
        },
        onSubmit: async (values)=>{
          await axios({
                method:"GET",
                url:"http://127.0.0.1:4000/getusers"
            }).then(response=>{
                for(var user of response.data){
                    if(user.UserName == values.UserName && user.Password==values.Password){
                        navigate("/categories");
                        break;
                    }
                    else{
                        navigate("/error");
                    }
                }  
            })
        }
    })

  return (
    <div className="container-fluid">
      <h2 className="ms-5">Login Page</h2>
      <form onSubmit={formik.handleSubmit} className="w-25">
        <dl>
          <dt>UserName</dt>
          <dd>
            <input
              type="text"
              name="UserName"
              onChange={formik.handleChange}
              className="form-control mb-2 mr-sm-2"
              placeholder="Jane Doe"
            />
          </dd>
          <dt>Password</dt>
          <dd>
            <input type="password"
            name="Password"
            onChange={formik.handleChange}
             className="form-control mb-2 mr-sm-2" />
          </dd>
        </dl>
        <button className="btn btn-primary w-100">Login</button>
      </form>
      <Link to="/register">New USer ?</Link>
    </div>
  );
}
