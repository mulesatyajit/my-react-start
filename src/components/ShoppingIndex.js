import React from 'react'
import './ShoppingIndex.css';
import { BrowserRouter, Link, Routes, Route } from 'react-router-dom'
import HomeComponent from './HomeComponent';
import CategoriesComponent from './CategoriesComponent';
import ProductsComponent from './ProductsComponent';
import DetailsComponent from './DetailsComponent';
import RegisterComponent from './RegisterComponent';
import LoginComponent from './LoginComponent';
import LoginErrorComponent from './LoginErrorComponent';
import ProductsIndex from '../CRUD/ProductsIndex';

export default function ShoppingIndex() {
    return (
        <div className='container-fluid'>
            <BrowserRouter>
                <header className='d-flex justify-content-between bg-dark text-white p-2'>
                    <div className='brand'>Shopper.</div>
                    <div>
                        <span><Link to="/home">Home</Link></span>
                        <span><Link to="/categories">Categories</Link></span>
                        <span><Link to="/register">Register</Link></span>
                        <span><Link to="/login">Login</Link></span>
                        <span><Link to="/products">Products</Link></span>
                    </div>
                    <div className='nav-icons'>
                        <span className='bi bi-search'></span>
                        <span className='bi bi-person'></span>
                        <span className='bi bi-heart'></span>
                        <span className='bi bi-cart4'></span>
                    </div>
                </header>
                <section>
                    <Routes>
                        <Route path="/home" element={<HomeComponent />}></Route>
                        <Route path='/categories' element={<CategoriesComponent />}></Route>
                        <Route path='/login' element={<LoginComponent/>}/>
                        <Route path='/register' element={<RegisterComponent/>}/>
                        <Route path='/error' element={<LoginErrorComponent/>}/>
                        <Route path='/products/:category' element={<ProductsComponent />}></Route>
                        <Route path='/details/:id/' element={<DetailsComponent />}></Route>
                        <Route path='/' element={<HomeComponent />}></Route>
                        <Route path='*' element={<h3><code>Page You Requested Not Found...</code></h3>}></Route>
                        <Route path='/products' element={<ProductsIndex/>}/>
                    </Routes>
                </section>
                <footer className='row bg-dark m-1 text-white p-2'>
                    <div className='col'>
                        <h2>Support</h2>
                        <ul className='list-unstyled'>
                            <li>Contact Us</li>
                            <li>FAQ</li>
                            <li>Size Guide</li>
                            <li>About Us</li>
                        </ul>
                    </div>
                    <div className='col'>
                        <h2>Support</h2>
                        <ul className='list-unstyled'>
                            <li>Contact Us</li>
                            <li>FAQ</li>
                            <li>Size Guide</li>
                            <li>About Us</li>
                        </ul>
                    </div>
                    <div className='col'>
                        <h2>Support</h2>
                        <ul className='list-unstyled'>
                            <li>Contact Us</li>
                            <li>FAQ</li>
                            <li>Size Guide</li>
                            <li>About Us</li>
                        </ul>
                    </div>
                    <div className='col'>
                        <h2>Support</h2>
                        <ul className='list-unstyled'>
                            <li>Contact Us</li>
                            <li>FAQ</li>
                            <li>Size Guide</li>
                            <li>About Us</li>
                        </ul>
                    </div>
                </footer>
            </BrowserRouter>
        </div>
    )
}
