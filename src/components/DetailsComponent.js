import React, { useEffect, useState } from 'react'
import { Link, useParams } from 'react-router-dom'

export default function DetailsComponent() {

    const param = useParams();

    const [detail, setDetails] = useState([]);

    useEffect(() => {
        fetch(`http://fakestoreapi.com/products/${param.id}`)
            .then(res => res.json())
            .then(data => {
                setDetails(data)
            })
    }, [param.id])


    return (
        <div>
            <h2>Details</h2>
            <dl>
                <dt>Name</dt>
                <dd>{detail.title}</dd>
                <dt>Price</dt>
                <dd>{detail.price}</dd>
                <dt>Preview</dt>
                <dd><img src={detail.image} width="100" height="100" className='m-2 p-2' border="1" alt="" /></dd>

            </dl>
            <Link to={'/products/' + detail.category}>Back to Products</Link>
        </div>
    )
}
