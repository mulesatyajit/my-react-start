import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom';

export default function CategoriesComponent() {

    const [categories, setCategories] = useState([]);

    useEffect(() => {
        fetch(`http://fakestoreapi.com/products/categories`)
            .then(res => res.json())
            .then(data => {
                setCategories(data);
            })
    }, [])

    return (
        <div>
            <h2>Categories Component</h2>
            <ul>
                {
                    categories.map(category => (
                        <li key={category}><Link to={'/products/' + category}>{category.toUpperCase()}</Link></li>
                    ))
                }
            </ul>
        </div>
    )
}
