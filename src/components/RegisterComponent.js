import React, { useState } from "react";
import { useFormik } from "formik";
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";

export default function RegisterComponent() {

    const [usrMsg, setUsrMsg] = useState('');
    const [erroClass, setErrorClass] = useState('');

  let navigate = useNavigate();
  const formik = useFormik({
    initialValues: {
      UserName: "",
      Password: "",
      Email: "",
      Mobile: "",
    },
    onSubmit: (values) => {
      // eslint-disable-next-line no-undef
      axios({
        method: "POST",
        url: "http://127.0.0.1:4000/registeruser",
        data: values,
      }).catch((err) => {
        console.log(err);
      });
      alert("Register Successfully");
      navigate("/login");
    },
  });
  function VerifyUser(e) {
    axios({
        method:"GET",
        url:"http://127.0.0.1:4000/getusers"
    }).then((response)=>{
        for(var user of response.data){
            if(user.UserName===e.target.value){
                setUsrMsg('User Name Taken - Try Another');
                setErrorClass('text-danger')
            }
            else{
                setUsrMsg("User Name Available");
                setErrorClass('text-success');
            }
        }
    })
  }

  return (
    <div className="container-fluid">
      <h2 className="ms-5">Register User</h2>
      <form onSubmit={formik.handleSubmit} className="w-25">
        <dl>
          <dt>UserName</dt>
          <dd>
            <input
              type="text"
              onKeyUp={VerifyUser}
              name="UserName"
              onChange={formik.handleChange}
              className="form-control mb-2 mr-sm-2"
              placeholder="Jane Doe"
            />
          </dd>
          <dd className={erroClass}>{usrMsg}</dd>
          <dt>Password</dt>
          <dd>
            <input
              type="password"
              name="Password"
              onChange={formik.handleChange}
              className="form-control mb-2 mr-sm-2"
            />
          </dd>
          <dt>Email</dt>
          <dd>
            <input
              type="text"
              name="Email"
              onChange={formik.handleChange}
              className="form-control mb-2 mr-sm-2"
              placeholder="Jane98@gmail.com"
            />
          </dd>
          <dt>Mobile</dt>
          <dd>
            <input
              type="text"
              name="Mobile"
              onChange={formik.handleChange}
              className="form-control mb-2 mr-sm-2"
              placeholder="+918765432166"
            />
          </dd>
          <button className="w-100 btn btn-primary">Reister</button>
          <p><Link to="/login">Existing User</Link></p>
        </dl>
      </form>
    </div>
  );
}
