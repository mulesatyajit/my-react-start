import React, { useEffect, useState } from 'react'
import { Link, useParams } from 'react-router-dom'

export default function ProductsComponent() {

    const params = useParams();
    const [categoryname, setCategoryName] = useState();
    const [products, setProducts] = useState([]);

    useEffect(() => {
        setCategoryName(params.category);
        fetch(`http://fakestoreapi.com/products/category/${params.category}`)
            .then(res => res.json())
            .then(data => {
                console.log(data);
                setProducts(data);
            })
    }, [params.category])

    return (
        <div>
            <h2>{categoryname} Products</h2>
            <div>
                {
                    products.map(product =>
                        <Link to={'/details/' + product.id}><img key={product.id} src={product.image} width="100" height="100" border="1" className="p-2 m-2" alt="" /></Link>
                    )
                }
            </div>
            <Link to='/categories'>Back to Categories</Link>
        </div>
    )
}
