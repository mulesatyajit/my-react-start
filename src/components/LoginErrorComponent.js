import React from 'react'
import { Link } from 'react-router-dom'

export default function LoginErrorComponent() {
  return (
    <div>
        <h2 className='text-danger'>Invalid User Name / Password</h2>
        <Link to="/login">Try again</Link>
    </div>
  )
}
