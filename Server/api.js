var mongoClient = require("mongodb").MongoClient;
var express = require("express");
var cors = require("cors");

var app = express();
app.use(cors());

app.use(express.urlencoded({
    extended: true
}));
app.use(express.json());

app.get("/", (req, res)=>{
    res.send("<h2>Shopping API</h2>");
});
app.get("/getusers",async (req, res)=>{
    const clientObj = await  mongoClient.connect("mongodb://127.0.0.1")
                var database = await clientObj.db("shopdb");
               const documents =  await database.collection("tblusers").find({}).toArray()
                        res.send(documents);
});

app.post("/registeruser", async (req, res)=>{
    var user = {
        "UserName": req.body.UserName,
        "Password": req.body.Password,
        "Email": req.body.Email,
        "Mobile": req.body.Mobile
    };
    const clientObj = await mongoClient.connect("mongodb://127.0.0.1")
                var database = await clientObj.db("shopdb");
               const inserted = await database.collection("tblusers").insertOne(user)
               console.log("Record Inserted")
})

app.listen(4000);
console.log("Server Started http://127.0.0.1:4000");