var mongoClient = require("mongodb").MongoClient;
var express = require("express");
var cors = require("cors");

var app = express();

app.use(cors());

app.use(express.urlencoded({
    extended: true
}));
app.use(express.json());

app.get("/",(req,res)=>{
    res.send("<h2>Products Data API</h2>")
})

app.get("/products",async(req,res)=>{
    const clientObj = await  mongoClient.connect("mongodb://127.0.0.1")
                var database = await clientObj.db("shopdb");
               const documents =  await database.collection("tblproducts").find({}).toArray()
                        res.send(documents);
})

app.post("/addproducts", async(req,res)=>{
    var product = {
        "ProductId": parseInt(req.body.ProductId),
        "Name": req.body.Name,
        "Price":parseFloat (req.body.Price),
        "Stock":(req.body.Stock=="true")?true:false
    }
    const clientObj = await mongoClient.connect("mongodb://127.0.0.1")
                var database = await clientObj.db("shopdb");
               const inserted = await database.collection("tblproducts").insertOne(product)
               console.log("Record Inserted")
})


app.listen(5000);
console.log("Server Started http://127.0.0.1:5000")